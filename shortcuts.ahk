; /////////////////////////////////////////
; Open or focus an application
;   string [app] Application name
;   string [path] Absolute path to an executable
; /////////////////////////////////////////
OpenOrFocusWindow(app, path) {
    IfWinNotExist ahk_exe %app%
        run, "%path%" 
    WinWait, ahk_exe %app%
    winactivate, ahk_exe %app%
}

; /////////////////////////////////////////
; Open or focus the explorer window
; /////////////////////////////////////////
OpenOrFocusExplorer() {
	IfWinNotExist ahk_class CabinetWClass
		Run explorer.exe
    	WinWait, ahk_class CabinetWClass
   	winactivate, ahk_class CabinetWClass
}

; /////////////////////////////////////////
; App shortcuts 
; /////////////////////////////////////////
^!w::OpenOrFocusWindow("chrome.exe", "C:\Program Files\Google\Chrome\Application\chrome.exe")
^!s::OpenOrFocusWindow("Code.exe", "C:\Programs\Microsoft VS Code\Code.exe")
^!d::OpenOrFocusWindow("blender.exe", "C:\Program Files\Blender Foundation\Blender 3.3\blender-launcher.exe")
^!a::OpenOrFocusWindow("UnrealEditor.exe", "C:\Program Files\Epic Games\UE_5.0\Engine\Binaries\Win64\UnrealEditor.exe")
^!e::OpenOrFocusWindow("Tabby.exe", "C:\Users\ds\AppData\Local\Programs\Tabby\Tabby.exe")
#e::OpenOrFocusExplorer()

; /////////////////////////////////////////
; Keybindings 
; /////////////////////////////////////////

!q::!F4